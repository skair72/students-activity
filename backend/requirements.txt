Django==3.0a1
psycopg2==2.8.3
daphne==2.3.0
aioredis==1.3.0
channels==2.3.0
channels-redis==2.4.0
asyncio==3.4.3