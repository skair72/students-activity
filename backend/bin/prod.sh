#!/usr/bin/env bash
/usr/local/bin/python manage.py migrate
/usr/local/bin/python manage.py collectstatic --noinput
#/usr/local/bin/python manage.py compilemessages
#/usr/local/bin/gunicorn --timeout 240 ddkolesnik.wsgi:application -w 2 -b :8000
#/bin/bash
/usr/local/bin/daphne -b 0.0.0.0 -p 8000 backend.asgi:application