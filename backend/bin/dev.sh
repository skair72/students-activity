#!/usr/bin/env bash
/usr/local/bin/python manage.py makemigrations --noinput
/usr/local/bin/python manage.py migrate
/usr/local/bin/python manage.py collectstatic --noinput
#/usr/local/bin/python manage.py compilemessages
/usr/local/bin/python manage.py runserver 0.0.0.0:8000
#/usr/local/bin/daphne -b 0.0.0.0 -p 8000 backend.asgi:application