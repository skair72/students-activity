import asyncio
import json

import aioredis
from channels.generic.websocket import AsyncWebsocketConsumer

predictions_channel = 'predictionsChannel'


# class RedisWatcher:
#     def __init__(self):
#         self.consumers = 0
#
#     async def open_connection(self):
#         pass
#
#     async def close_connection(self):
#         pass


async def reader(ch, callback):
    while await ch.wait_message():
        msg = await ch.get()
        await callback(msg)


class ChatConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print('start initializiting!')
        self.redis = None

    async def connect(self):
        self.redis = await aioredis.create_redis('redis://redis:6379/0')
        res = await self.redis.subscribe(predictions_channel)
        ch1 = res[0]
        asyncio.ensure_future(reader(ch1, self.on_redis_message))

        await self.accept()

    async def on_redis_message(self, new_message):
        # print(f'GOT {new_message}')
        await self.send(bytes_data=new_message)

    async def disconnect(self, close_code):
        await self.redis.unsubscribe(predictions_channel)
        self.redis.close()
        await self.redis.wait_closed()
        await self.close(close_code)

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        pass
        # text_data_json = json.loads(text_data)
        # message = text_data_json['message']
        #
        # # Send message to room group
