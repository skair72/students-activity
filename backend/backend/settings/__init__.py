import os

if bool(os.getenv('IS_DEBUG', False)):
    from .development import *
else:
    from .production import *
