# You have to provide registration-token as first positional argument
# And tag name as second positional argument

gitlab_runner_config_name="gitlab-runner-config-$2"
gitlab_runner_name="gitlab-runner-$2"

docker stop "${gitlab_runner_config_name}" && docker rm "${gitlab_runner_config_name}"
docker run -d --name "${gitlab_runner_config_name}" \
    -v /etc/gitlab-runner \
    busybox:latest \
    /bin/true

docker stop "${gitlab_runner_name}" && docker rm "${gitlab_runner_name}"
docker run -d --name "${gitlab_runner_name}" --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --volumes-from "${gitlab_runner_config_name}" \
    gitlab/gitlab-runner:alpine

docker run --rm --volumes-from "${gitlab_runner_config_name}" gitlab/gitlab-runner:alpine register \
  --non-interactive \
  --tag-list "docker-compose,$2" \
  --run-untagged="true" \
  --url "https://git.crtweb.ru/" \
  --registration-token "$1" \
  --executor docker \
  --description "docker-runner-$2" \
  --docker-image "alpine" \
  --docker-volumes "/builds:/builds:rw" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

docker exec "${gitlab_runner_name}" cat /etc/gitlab-runner/config.toml