#!/bin/bash
# You must provide two positional arguments COMPOSE_PROJECT_NAME and SERVER_PORT
ENV_PATH=.env
if [[ ! -f "$ENV_PATH" ]]; then

    function get_random {
        echo `openssl rand -base64 16`
    }

    touch `pwd`'/.env'
    echo "SECRET_KEY=$(get_random)
POSTGRES_DB=$(get_random)
POSTGRES_USER=$(get_random)
POSTGRES_PASSWORD=$(get_random)
POSTGRES_HOST=db
POSTGRES_PORT=5432
DJANGO_STATIC_URL=/static
DJANGO_MEDIA_URL=/media
COMPOSE_PROJECT_NAME=$1
SERVER_PORT=$2
" > $ENV_PATH
fi

echo 'Done.'