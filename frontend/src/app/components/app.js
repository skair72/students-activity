import React, {useEffect, useState} from 'react';
import useWebSocket from 'react-use-websocket';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    card: {
        minWidth: 275,
        margin: theme.spacing(1)
    },
    title: {
        fontSize: 14,
    },
    centered: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

function App() {
    const reader = new FileReader();
    const [sendMessage, lastMessage, readyState] = useWebSocket('ws://127.0.0.1:8000/ws/');
    const [predictions, setPredictions] = useState([]);
    const classes = useStyles();

    reader.addEventListener('loadend', (e) => {
        try {
            setPredictions(JSON.parse(e.target.result))
        } catch (e) {
            console.log(e)
        }
    });

    useEffect(() => {
        console.log(`lastMessage is ${lastMessage}`);
        if (lastMessage !== null)
            reader.readAsText(lastMessage.data);
        return () => {
            reader.abort()
        }
    }, [lastMessage]);

    return (
        <div className={classes.centered}>
            <Box
                display="flex"
                flexWrap="wrap"
                bgcolor="background.paper"
            >
                {
                    predictions.length > 0 ?
                        predictions.map(prediction =>
                            <Card key={prediction.id} className={classes.card}>
                                <CardContent>
                                    {
                                        prediction.markers.map((marker) =>
                                            <Typography variant="h5" component="h2" key={Object.keys(marker)[0]}>
                                                {`${Object.keys(marker)[0]}: ${Object.values(marker)[0]}%`}
                                            </Typography>
                                        )
                                    }
                                </CardContent>
                            </Card>
                        ) : <Card className={classes.card}>
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    No humans
                                </Typography>
                            </CardContent>
                        </Card>
                }
            </Box>
        </div>

    );
}

export default App;
