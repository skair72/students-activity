import json
import random
import time

import redis

r = redis.Redis(host='redis', port=6379, db=0)
prediction_status_channel = 'predictionsChannel'
predictions_set_name = 'predictions'
frequency = 3
markers = ['Вовлеченность', 'Внимание', 'Активность']

while True:
    time.sleep(frequency)
    number_of_persons = random.randint(1, 3)
    predictions = [{
        'id': i,
        'markers': [
            {
                j: round(random.uniform(0, 100), 2)
            } for j in markers
        ]
    }
        for i in range(number_of_persons)]

    predictions_string = json.dumps(predictions, ensure_ascii=False).encode('utf8')
    # r.set(predictions_set_name, predictions_string)
    r.publish(prediction_status_channel, predictions_string)
    print(f'predictions_string: {predictions_string}')
